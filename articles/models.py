from django.db import models
from pygments.lexers import get_all_lexers
from pygments.styles import get_all_styles
from pygments.formatters.html import HtmlFormatter
from pygments import highlight

LEXERS = [item for item in get_all_lexers() if item[1]]
LANGUAGE_CHOICES = sorted([(item[1][0], item[0]) for item in LEXERS])
STYLE_CHOICES = sorted((item, item) for item in get_all_styles())


class Article(models.Model):
    title = models.CharField(max_length=100, blank=True, default='')
    description = models.TextField()
    url = models.CharField(max_length=200,default='')
    owner = models.ForeignKey('auth.User', related_name='articles', on_delete=models.CASCADE)
    highlighted = models.TextField()
    type = models.TextField(max_length=4,default='')

def save(self, *args, **kwargs):
    """
    Use the `pygments` library to create a highlighted HTML
    representation of the code snippet.
    """
    lexer = get_lexer_by_name(self.language)
    linenos = self.linenos and 'table' or False
    options = self.title and {'title': self.title} or {}
    formatter = HtmlFormatter(style=self.style, linenos=linenos,
                              full=True, **options)
    self.highlighted = highlight(self.code, lexer, formatter)
    super(Article, self).save(*args, **kwargs)

class Version(models.Model):
    version = models.IntegerField()