from articles import views

from articles.admin import admin
from django.conf.urls import url,include
from rest_framework.urlpatterns import format_suffix_patterns

urlpatterns = [
    url(r'^articles/$', views.ArticleList.as_view()),
    url(r'^articles_delete/$', views.ArticleListDestroy.as_view()),
    url(r'^articles/(?P<pk>[0-9]+)/$', views.ArticleDetail.as_view()),
    url(r'^version/(?P<pk>[0-9]+)/$', views.VersionDetail.as_view()),
    url(r'^users/$', views.UserList.as_view()),
    url(r'^users/(?P<pk>[0-9]+)/$', views.UserDetail.as_view()),
	url(r'^admin/',admin.site.urls),
]

urlpatterns = format_suffix_patterns(urlpatterns)

urlpatterns += [
    url(r'^api-auth/', include('rest_framework.urls',
                               namespace='rest_framework')),
]