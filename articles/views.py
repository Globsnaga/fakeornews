from articles.models import Article,Version
from articles.permissions import IsOwnerOrReadOnly
from articles.serializers import ArticleSerializer
from articles.serializers import UserSerializer,VersionSerializer
from django.contrib.auth.models import User
from rest_framework import generics,permissions


class ArticleList(generics.ListCreateAPIView):
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)
    queryset = Article.objects.all()
    serializer_class = ArticleSerializer
    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)
    def perform_destroy(self, instance):
        Article.objects.all().delete()

class ArticleListDestroy(generics.DestroyAPIView):
    permission_classes = (permissions.IsAuthenticated,)
    queryset = Article.objects.all()
    serializer_class = ArticleSerializer
    def perform_destroy(self,instance):
        Article.objects.all().delete()

class ArticleDetail(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,IsOwnerOrReadOnly,)
    queryset = Article.objects.all()
    serializer_class = ArticleSerializer

class UserList(generics.ListAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer


class UserDetail(generics.RetrieveAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer

class VersionDetail(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)
    queryset = Version.objects.all()
    serializer_class = VersionSerializer
